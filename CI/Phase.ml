type t =
| Pos 
| Neg

let of_nperm nperm =
  if (nperm land 1) = 1 then Neg
  else Pos

let to_nperm = function
| Pos -> 0
| Neg -> 1

let add t t' =
  match t, t' with
  | Pos, Pos 
  | Neg, Neg -> Pos
  | Pos, Neg 
  | Neg, Pos -> Neg

let neg = function
  | Pos -> Neg
  | Neg -> Pos

let add_nperm phase = function
  | 0 -> phase
  | nperm -> add phase (of_nperm nperm)

let pp ppf = function
  | Pos -> Format.fprintf ppf "@[<h>+1@]"
  | Neg -> Format.fprintf ppf "@[<h>-1@]"


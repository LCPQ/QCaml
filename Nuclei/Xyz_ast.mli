(** When an [xyz] file is read by the [Xyz_parser.mly], it is converted into
 an {!xyz_file} data structure. *)

type nucleus =
  {
    element: Element.t ;
    coord  : Coordinate.angstrom Coordinate.point;
  }

type xyz_file =
  {
    number_of_atoms : int ;
    file_title      : string ;
    nuclei          : nucleus list ;
  }


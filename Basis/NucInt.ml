(** Electron-nucleus repulsion integrals *)

open Util
open Constants
open Lacaml.D

type t = Mat.t
external matrix : t -> Mat.t = "%identity"
external of_matrix : Mat.t -> t = "%identity"

module Am = AngularMomentum
module Bs = Basis
module Cs = ContractedShell

(** (0|0)^m : Fundamental electron-nucleus repulsion integral
    $ \int \phi_p(r1) 1/r_{C} dr_1 $

    maxm        : Maximum total angular momentum
    expo_pq_inv : $1./p + 1./q$ where $p$ and $q$ are the exponents of
                $\phi_p$ and $\phi_q$
    norm_pq_sq  : square of the distance between the centers of $\phi_p$
                and $\phi_q$
*)

let zero_m ~maxm ~expo_pq_inv ~norm_pq_sq =
  let exp_pq = 1. /. expo_pq_inv in
  let t = norm_pq_sq *. exp_pq in
  boys_function ~maxm t
  |> Array.mapi (fun m fm ->
      two_over_sq_pi *. fm *.
      (pow exp_pq m) *. (sqrt exp_pq)
    )


(** Compute all the integrals of a contracted class *)
let contracted_class_shell_pair shell_p geometry: float Zmap.t =
  OneElectronRR.contracted_class_shell_pair ~zero_m shell_p geometry


let cutoff = integrals_cutoff

let cutoff2 = cutoff *. cutoff
exception NullIntegral


let of_basis_nuclei ~basis nuclei =
  let to_powers x = 
    let open Zkey in
    match to_powers x with
    | Three x -> x
    | _ -> assert false
  in

  let n     = Bs.size basis
  and shell = Bs.contracted_shells basis
  in

  let eni_array = Mat.create n n in

  (* Pre-compute all shell pairs *)
  let shell_pairs =
    Array.mapi (fun i shell_a -> Array.map (fun shell_b -> 
        ContractedShellPair.make shell_a shell_b) (Array.sub shell 0 (i+1)) ) shell
  in

  (* Compute Integrals *)
  for i=0 to (Array.length shell) - 1 do
    for j=0 to i do
      match shell_pairs.(i).(j) with
      | None -> ()
      | Some shell_p ->
        (* Compute all the integrals of the class *)
        let cls =
          contracted_class_shell_pair shell_p nuclei
        in

        (* Write the data in the output file *)
        Array.iteri (fun i_c powers_i ->
            let i_c = Cs.index shell.(i) + i_c + 1 in
            let xi = to_powers powers_i in
            Array.iteri (fun j_c powers_j ->
                let j_c = Cs.index shell.(j) + j_c + 1 in
                let xj = to_powers powers_j in
                let key = 
                  Zkey.of_powers_six xi xj
                in
                let value = 
                  Zmap.find cls key 
                in
                eni_array.{j_c,i_c} <- value;
                eni_array.{i_c,j_c} <- value;
              ) (Am.zkey_array (Singlet (Cs.ang_mom shell.(j))))
          ) (Am.zkey_array (Singlet (Cs.ang_mom shell.(i))))
    done;
  done;
  Mat.detri eni_array;
  eni_array


let to_file ~filename eni_array =
  let n = Mat.dim1 eni_array in
  let oc = open_out filename in

  for j=1 to n do
    for i=1 to j do
      let value = eni_array.{i,j} in
      if (value <> 0.) then
        Printf.fprintf oc " %5d %5d %20.15f\n" i j value;
    done;
  done;
  close_out oc


let of_basis basis =
  invalid_arg "of_basis_nuclei should be called for NucInt"

let of_basis_pair basis =
  failwith "Not implemented"


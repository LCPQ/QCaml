#!/bin/sh

# --------------------------------
# Defaults

package_name="QCaml"
prefix='/usr/local'
bin='$(prefix)/bin'
lib='$(prefix)/lib'
doc='$(prefix)/doc'
share='$(prefix)/share'
man='$(prefix)/man'
etc='$(prefix)/etc'
ocamlcflags='"-g -warn-error A -cc gcc -ccopt -Ofast -ccopt -march=native -ccopt=-fPIC "'
ocamllflags='"-g -warn-error A"'
ocamloptflags='"opt -O3 -nodynlink -remove-unused-arguments -rounds 16 -inline 100 -inline-max-unroll 100 -cc gcc -ccopt -Ofast -ccopt -march=native -ccopt=-fPIC "'
ocamldocflags='-docflags "-g ltxhtml.cma -sort -css-style $(PWD)/style.css -colorize-code"'
mpi=''
ocamloptflags_profile='"opt -p -cc gcc -ccopt -Ofast -ccopt -march=native -ccopt -no-pie "'

# --------------------------------


LC_ALL=C
export LC_ALL

if [ x.$OPAM_PACKAGE_NAME != x. ] ; then
  package_name=$OPAM_PACKAGE_NAME
fi


help()
{
    cat <<EOF
usage: configure [options]

where options include:
  -prefix dir          installation directory
  -bin dir             default: $bin
  -lib dir             default: $lib
  -doc dir             default: $doc
  -share dir           default: $share
  -man dir             default: $man
  -etc dir             default: $etc
  -ocamlcflags         default: $ocamlcflags
  -ocamloptflags       default: $ocamloptflags
  -mpi                 default: mpi off
  -profile             default: off

EOF
    exit
}


while : ; do
    case "$1" in
        "")
            break;;
        -prefix|--prefix)
            prefix="$2"
            shift;;
        -bin|--bin)
            bin="$2"
            shift;;
        -lib|--lib)
            lib="$2"
            shift;;
        -doc|--doc)
            doc="$2"
            shift;;
        -etc|--etc)
            etc="$2"
            shift;;
        -man|--man)
            man="$2"
            shift;;
        -share|--share)
            share="$2"
            shift;;
        -mpi|--mpi)
            mpi='-tag "package(mpi)"';;
        -profile|--profile)
            ocamloptflags=$ocamloptflags_profile;;
        -h|-help|--help)
            help;;
        *)
            echo "Unknown option $1, try -help"
            exit 2;;
    esac
    shift
done

rm -f Parallel
if [ "x.$mpi" != "x." ] ; then
  ln -s Parallel_mpi Parallel
else
  ln -s Parallel_serial Parallel
fi

cat << EOF > Makefile
package_name=$package_name
prefix=$prefix
bin=$bin
lib=$lib
doc=$doc
share=$share
man=$man
etc=$etc
mpi=$mpi

ocamlcflags=$ocamlcflags
ocamllflags=$ocamllflags
ocamloptflags=$ocamloptflags
ocamldocflags=$ocamldocflags


include Makefile.include
EOF


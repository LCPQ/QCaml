(** Overlap matrix of the atomic orbitals.

{% $$ \langle \chi_i | \chi_j \rangle $$ %}
*)

include module type of MatrixOnBasis

(**
The space built with determinants made with same-spin spinorbitals.
*)

type t

(** {1 Accessors} *)

val size : t -> int
(** Number of determinants in the space. *)

val spin_determinants : t -> Spindeterminant.t array
(** All the spin-determinants belonging to the space. *)

val elec_num : t -> int
(** Number of (same-spin) electrons occupying the MOs. *)

val mo_class : t -> MOClass.t
(** The MO classes used to generate the space. *)

val mo_basis : t -> MOBasis.t
(** The MO basis on which the determinants are expanded. *)


(** {1 Creation} *)

val fci_of_mo_basis : frozen_core:bool -> MOBasis.t -> int -> t
(** Create a space of all possible ways to put [n_elec-ncore] electrons in the
    [Active] MOs.  All other MOs are untouched.
*)

val cas_of_mo_basis : MOBasis.t -> frozen_core:bool -> int -> int -> int -> t
(** [cas_of_mo_basis mo_basis n_elec n m] creates a CAS(n,m) space of 
    [Active] MOs. The unoccupied MOs are [Virtual], and the occupied MOs
    are [Core] and [Inactive].
*)


(** {2 Printing} *)

val pp : Format.formatter -> t -> unit






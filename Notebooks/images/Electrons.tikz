﻿% Documentation
% -------------
%
% \up      : up   electron (black)
% \upr     : up   electron (red)
% \dn      : down electron (black)
% \dnr     : down electron (red)
% \updn    : up   electron (black), down electron (black)
% \updnrr  : up   electron (red  ), down electron (red  )
% \updnrb  : up   electron (red  ), down electron (black)
% \updnbr  : up   electron (black), down electron (red  )
% \emp     : empty orbital
% \cabs{ } : CABS space inside
%
% Example
% -------
%
% \input{Electrons.tikz}
%
% \cabs{
%   \emp{0.}{2.0}
%   \emp{0.}{1.5}
% }
% \emp{0.}{1.0}
% \updnrb{0.}{0.5}
% \updn{0.}{0.}
%

% Electron symbol
\newcommand{\upel}{$\uparrow$}
\newcommand{\dnel}{$\downarrow$}

% Colors
\newcommand{\cabs}[1]{ { \color{lightgray}{#1} } }
\newcommand{\obs}[1]{  { \color{black}{#1} } }
\newcommand{\exca}[1]{  { \color{red}{#1} } }
\newcommand{\excb}[1]{  { \color{Cyan}{#1} } }
\newcommand{\excc}[1]{  { \color{Green}{#1} } }
\newcommand{\noexc}[1]{{ \color{black}{#1} } }

% Spacing between the 3 diagrams
\newcommand{\XI}{-1.}
\newcommand{\XA}{0.}
\newcommand{\XJ}{1.}

% Up electron
\newcommand{\up}[2]{
  \draw [-,thick] (-0.2+#1,#2) -- (0.2+#1,#2);
  \node at (#1-0.1,#2) {\noexc{\upel}};
}

\newcommand{\upa}[2]{
  \draw [-,thick] (-0.2+#1,#2) -- (0.2+#1,#2);
  \node at (#1-0.1,#2) {\exca{\upel}};
}

\newcommand{\upb}[2]{
  \draw [-,thick] (-0.2+#1,#2) -- (0.2+#1,#2);
  \node at (#1-0.1,#2) {\excb{\upel}};
}

\newcommand{\upc}[2]{
  \draw [-,thick] (-0.2+#1,#2) -- (0.2+#1,#2);
  \node at (#1-0.1,#2) {\excc{\upel}};
}

% Down electron
\newcommand{\dn}[2]{
  \draw [-,thick] (-0.2+#1,#2) -- (0.2+#1,#2);
  \node at (#1+0.1,#2) {\noexc{\dnel}};
}

\newcommand{\dna}[2]{
  \draw [-,thick] (-0.2+#1,#2) -- (0.2+#1,#2);
  \node at (#1+0.1,#2) {\exca{\dnel}};
}

\newcommand{\dnb}[2]{
  \draw [-,thick] (-0.2+#1,#2) -- (0.2+#1,#2);
  \node at (#1+0.1,#2) {\excb{\dnel}};
}

\newcommand{\dnc}[2]{
  \draw [-,thick] (-0.2+#1,#2) -- (0.2+#1,#2);
  \node at (#1+0.1,#2) {\excc{\dnel}};
}

% Up and Down electrons
\newcommand{\updn}[2]{
  \draw [-,thick] (-0.2+#1,#2) -- (0.2+#1,#2);
  \node at (#1+0.1,#2) {\noexc{\dnel}};
  \node at (#1-0.1,#2) {\noexc{\upel}};
}

\newcommand{\updnab}[2]{
  \draw [-,thick] (-0.2+#1,#2) -- (0.2+#1,#2);
  \node at (#1+0.1,#2) {\excb{\dnel}};
  \node at (#1-0.1,#2) {\exca{\upel}};
}

\newcommand{\updncb}[2]{
  \draw [-,thick] (-0.2+#1,#2) -- (0.2+#1,#2);
  \node at (#1+0.1,#2) {\excb{\dnel}};
  \node at (#1-0.1,#2) {\excc{\upel}};
}

\newcommand{\updna}[2]{
  \draw [-,thick] (-0.2+#1,#2) -- (0.2+#1,#2);
  \node at (#1+0.1,#2) {\noexc{\dnel}}; 
  \node at (#1-0.1,#2) {\exca{\upel}};
}

\newcommand{\updnb}[2]{
  \draw [-,thick] (-0.2+#1,#2) -- (0.2+#1,#2);
  \node at (#1-0.1,#2) {\noexc{\upel}}; 
  \node at (#1+0.1,#2) {\excb{\dnel}};
}

\newcommand{\updnc}[2]{
  \draw [-,thick] (-0.2+#1,#2) -- (0.2+#1,#2);
  \node at (#1+0.1,#2) {\noexc{\dnel}}; 
  \node at (#1-0.1,#2) {\excc{\upel}};
}

% Empty orbital
\newcommand{\emp}[2]{
  \draw [-,thick] (-0.2+#1,#2) -- (0.2+#1,#2);
}

% Determinant label
\node at (\XI,-0.7) {$\ket{I}$};
\node at (\XA,-0.7) {$\ket{\alpha}$};
\node at (\XJ,-0.7) {$\ket{J}$};

\newcommand{\XX}{0.}

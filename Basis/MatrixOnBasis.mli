(** Signature for matrices built on the {!Basis.t} *)

open Lacaml.D

type t 

val of_basis : Basis.t -> t
(** Build from a {!Basis.t}. *)

val of_basis_pair : Basis.t -> Basis.t -> t
(** Build from a pair of {!Basis.t}. *)

val to_file : filename:string -> t -> unit
(** Write the integrals in a file. *)

val matrix : t -> Mat.t
(** Returns the matrix suitable for Lacaml. *)

val of_matrix : Mat.t -> t 
(** Build from a Lacaml matrix. *)

(** Electronic kinetic energy integrals, expressed as a matrix in a {!Basis.t}.

{%
$$
T_{ij} = \left \langle \chi_i \left| -\frac{1}{2} \Delta \right| \chi_j \right \rangle
$$
%}
*)

include module type of MatrixOnBasis



type t

val nuclei : t -> Nuclei.t
(** Nuclear coordinates used in the smiulation *)

val charge : t -> Charge.t
(** Total charge (electrons + nuclei) *)

val electrons : t -> Electrons.t
(** Electrons used in the simulation *)

val basis : t -> Basis.t
(** Basis set used to build the AOs *)

val ao_basis : t -> AOBasis.t
(** Atomic basis set *)

val nuclear_repulsion : t -> float
(** Nuclear repulsion energy *)

val f12 : t -> F12factor.t option
(** f12 correlation factor *)

val range_separation : t -> float option
(** Range-separation parameter of the electron repulsion integrals potential *)

(** {1 Creation} *)

val make :
  ?cartesian:bool ->
  ?multiplicity:int ->
  ?charge:int -> 
  nuclei:Nuclei.t ->
  Basis.t -> t

val of_filenames :
  ?cartesian:bool ->
  ?multiplicity:int ->
  ?charge:int ->
  ?f12:F12factor.t -> 
  ?range_separation: float ->
  nuclei:string ->
  ?aux_basis_filenames:string list ->
  string -> t


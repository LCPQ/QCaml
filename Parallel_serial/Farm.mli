(** The Farm skeleton, similar to SklMl.

The input is a stream of input data, and the output is a stream of data.
*)


val run : ?ordered:bool -> ?comm:'c ->
  f:('a -> 'b) -> 'a Stream.t -> 'b Stream.t
(** Run the [f] function on every process by popping elements from the
    input stream, and putting the results on the output stream. If [ordered]
    (the default is [ordered = true], then the order of the output is kept
    consistent with the order of the input.
    In the non-parallel mode, the [comm] argument is unused.
*)


